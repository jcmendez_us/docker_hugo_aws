# Hugo and aws-cli image

This is a Docker image to build static websites using hugo and push
them to aws.  It also includes grunt for the asset pipeline

[![DockerHub Badge](http://dockeri.co/image/jcmendez/hugo_aws_builder)](https://hub.docker.com/r/jcmendez/hugo_aws_builder/)

## Build

```
docker build -t jcmendez/hugo_aws_builder .
```

## Usage

On your development machine, on the directory where the hugo site is

```
docker run --rm --volume="$PWD:/srv/hugo" -p 35729:35729 -p 1313:1313 -it jcmendez/hugo_aws_builder:v2.2 hugo server -s /srv/hugo --watch --verbose
```

If you are using gulp for your asset pipeline, you may want to include a
shell script similar to this one (assuming `gulp watch` is the gulp task you
set up to watch and build your pipeline)

```
#!/bin/sh

cd "$(dirname "$0")"
hugo server --verbose &
gulp watch &
wait
```



To build the site

```
docker run --rm --volume="$PWD:/srv/hugo" -it jcmendez/hugo_aws_builder:v1 hugo build -s /srv/hugo
```

AWS needs the following ENV variables, that need to be set

```
AWS_ACCESS_KEY_ID
AWS_SECRET_ACCESS_KEY
AWS_DEFAULT_REGION
```

## Similar projects

https://github.com/mesosphere/aws-cli

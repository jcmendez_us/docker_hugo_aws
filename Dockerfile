FROM node:11.10-alpine
MAINTAINER Juan C. Mendez <jcmendez@alum.mit.edu>

ENV HUGO_VERSION 0.54.0
ENV HUGO_BINARY hugo_${HUGO_VERSION}_linux-64bit

RUN echo "http://dl-2.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories
RUN apk --no-cache update && \
    apk add --no-cache dumb-init sassc \
                       python python-dev py-pip py-setuptools \
                       py-pygments ca-certificates \
                       curl groff && \
    pip install --upgrade pip && \
    pip --no-cache-dir install awscli && \
    rm -r /root/.cache && \
    rm -rf /var/cache/apk/*

# Install the gulp command
RUN npm install gulp-cli postcss-cli autoprefixer -g

# Download and Install hugo
ADD https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/${HUGO_BINARY}.tar.gz /usr/local/hugo/
RUN tar xzf /usr/local/hugo/${HUGO_BINARY}.tar.gz -C /usr/local/hugo/ \
    && ln -s /usr/local/hugo/hugo /usr/local/bin/hugo \
    && rm /usr/local/hugo/${HUGO_BINARY}.tar.gz

EXPOSE 1313
EXPOSE 35729

CMD hugo version
